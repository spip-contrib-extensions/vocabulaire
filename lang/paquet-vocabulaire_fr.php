<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'dictionnaire_fr_description' => '',
	'dictionnaire_fr_nom' => 'Dictionnaire français',
	'dictionnaire_fr_slogan' => 'Liste des mots français',
);